class Fighter {

    constructor (name = 'Minion', health = 1, attack = 1, defense = 1) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
    }

    getHitPower() {
        let criticalHitChance = (Math.random() * 2) + 1;
        const power = this.attack * criticalHitChance;
        return power;
    }

    getBlockPower() {
        let dodgePower = (Math.random() * 2) + 1;
        const power = this.defense * dodgePower;
        return power;
    }

    kick(versusFighter) {
        versusFighter.health = versusFighter.health - (this.getHitPower() - versusFighter.getBlockPower());
    }
}

export default Fighter;

