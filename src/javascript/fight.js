function fight (fighter1, fighter2) {
    while (fighter1.health > 0 || fighter2.health >0) {
        fighter1.kick(fighter2);
        fighter2.kick(fighter1);
    }

    if (fighter1.health <= 0) {
        console.log(fighter2.name + 'wins!');
    } else if (fighter2.health <= 0) {
        console.log(fighter1.name + 'wins!');
    } else if (fighter1.health <= 0 && fighter2.health <=0) {
        console.log('Friendship wins');
    }
}

export {fight};